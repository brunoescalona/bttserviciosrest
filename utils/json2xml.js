/**
 * Convert the JSON response to XML
 */
var Json2xml = function (json){
	var xml;
	// Write the head of the xml
	xml = '<?xml version="1.0" encoding="UTF-8"?>';
	// OPEN Element gpx
	xml += '<gpx creator="BTT Alcoi - http://www.blog-bttalcoi.rhcloud.com" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">';
	// OPEN Element trk
	xml +='<trk>';
	// OPEN Element name
	xml +='<name>';
	xml +=json.nombre;
	// CLOSE Element name
	xml +='</name>';
	// OPEN Element desc
	xml +='<desc>';
	xml +=json.descripcion;	
	// CLOSE Element name
	xml +='</desc>';
	// OPEN Element trkseg
	xml +='<trkseg>';
	for (var position = 0; position<json.coordenadas.length; position++){
		// OPEN Element trkpt
		xml += '<trkpt lat="'+ json.coordenadas[position].latitud +
			   '" lon="'+ json.coordenadas[position].longitud +'">';
		// OPEN Element ele
		xml +='<ele>';
		xml += json.coordenadas[position].altitud;
		// CLOSE Element ele
		xml +='</ele>';
		// CLOSE Element trkpt
		xml += '</trkpt>';
	}
	// CLOSE Element trkseg
	xml +='</trkseg>';
	// CLOSE Element trk
	xml +='</trk>';	
	// CLOSE Element gpx
	xml += '</gpx>';
	
	return xml;
};

module.exports = Json2xml;