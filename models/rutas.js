/**
 * Model file RUTAS
 */
//Load mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Create a schema
var rutaSchema = new Schema({
	nombre: String,
	descripcion: String,
	dificultad: String,
	distancia: Number,
	esCircular: Boolean,
	coordenadas:[{
		_id:false,
		latitud: String,
		longitud: String,
		altitud: String,
		precision: String
	}]},
	{
	versionKey:false
})

//Model to use it
var Ruta = mongoose.model('Ruta', rutaSchema);

//Export
module.exports = Ruta;