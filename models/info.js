/**
 * Model file RUTAS
 */
//Load mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Create a schema
var infoSchema = new Schema({
	tipo: String,
	nombre: String,
	telf: Number,
	icono: String,
	direccion:String,
	},
	{
	versionKey:false
})

//Model to use it
var Info = mongoose.model('Info', infoSchema);

//Export
module.exports = Info;