//create bd
var rutas = new Rutas();
var infos = new Infos();

$(document).ready(function() {
	infos.fetch().done(function(){
		var viewEditInfo = new ViewEditInfo({
			collection : infos,
			el : '#editar-info'
		});
	});
});

$(document).ready(function() {
	infos.fetch().done(function(){
		var viewCreateInfo = new ViewCreateInfo({
			collection : infos,
			el : '#nueva-info'
		});
	});
});

$(document).ready(function() {
	rutas.fetch().done(function(){
		var viewCreateRuta = new ViewCreateRuta({
			collection : rutas,
			el : '#nueva-ruta'
		});
	});
});

$(document).ready(function() {
	rutas.fetch().done(function(){
		var viewEditRuta = new ViewEditRuta({
			collection : rutas,
			el : '#editar-ruta'
		});
	});
});


