/**
 * Define a info collection
 */
var Infos = Backbone.Collection.extend({
	url : INFO_END_POINT,
	model : Info,
	parse : function(response) {
		// parse the JSON object "info" into the array [Info]
		return response.info;
	},
	initialize : function() {
		//Method on add
		this.on('add', function(model, col, opt) {
			model.save(model,{
			        success: function(model, response) {
			        	Materialize.toast(INFO_SAVED_CORRECT, 2000);
			        },
			        error: function(model, response) {
			        	Materialize.toast(INFO_SAVED_INCORRECT, 2000);
			        },
			        wait: true
			});
		});
		//Method on remove
		this.on('remove', function(model, col, opt) {
			model.destroy({
				 success: function(model, response) {
			        	Materialize.toast(INFO_DELETED_CORRECT, 2000);
			        },
			        error: function(model, response) {
			        	Materialize.toast(INFO_DELETED_INCORRECT, 2000);
			        },
			        wait: true
			});
		});
		//Fetch the data from the server
		this.fetch({
			reset : true
		});

	}
});
