/**
 * Ruta implementation
 */
var Ruta = Backbone.Model.extend({
	urlRoot: RUTAS_END_POINT,
	defaults : {
		nombre : '',
		descripcion : '',
		dificultad : 'Facil',
		distancia:0.0,
		esCircular:false,
		coordenadas:[{}]
	},
	idAttribute:"_id"
});
