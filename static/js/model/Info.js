/**
 * Info implementation
 */
var Info = Backbone.Model.extend({
	urlRoot: INFO_END_POINT,
	defaults : {
		nombre: '',
		tipo : '',
		telf : 0,
		icono : '',
		direccion:'',
	},
	idAttribute:"_id"
});
