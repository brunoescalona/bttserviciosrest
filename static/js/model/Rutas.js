/**
 * Define a rutas collection
 */
var Rutas = Backbone.Collection.extend({
	url : RUTAS_END_POINT,
	model : Ruta,
	parse : function(response) {
		return response.rutas;
	},
	initialize : function() {
		this.on('add', function(model, col, opt) {
			model.save(model,{
		        success: function(model, response) {
		        	this.$('#progress-create').addClass('hide');
		        	Materialize.toast(RUTA_SAVED_CORRECT, 2000);
		        },
		        error: function(model, response) {
		        	Materialize.toast(RUTA_SAVED_INCORRECT, 2000);
		        },
		        wait: true
			}); 
		});
			
		this.on('remove', function(model, col, opt) {
			model.destroy({
				 success: function(model, response) {
			        	Materialize.toast(RUTA_DELETED_CORRECT, 2000);
			        },
			        error: function(model, response) {
			        	Materialize.toast(RUTA_DELETED_INCORRECT, 2000);
			        },
			        wait: true
			});
		});
		
		this.fetch({
			reset : true
		});

	}
});