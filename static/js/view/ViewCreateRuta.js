/**
 * View Create Ruta
 */
var finalRuta;
var routePath;

var ViewCreateRuta = Backbone.View.extend({
	initialize : function() {
		var self = this;
		this.collection.on('remove', function() {
			self.render();
		});
		this.collection.on('sync', function() {
			self.render();
		});
		self.render();
	},

	// Render all in blank after submit
	render : function() {
		this.$('#name').val('');
		this.$('#ruta-description').val('');
		this.$('#ruta-dificult').val('Facil');
		// re-initialize material input
		this.$('#ruta-dificult').material_select();
		this.$('#ruta-circular').attr('checked','checked');
		// re-initialize material text fields
		Materialize.updateTextFields();
		this.$('#button-guardar')
				.removeClass("waves-effect waves-light submit").addClass(
						'disabled');
		// Call the select item to initialize
		$(document).ready(function() {
			$('select').material_select();
		});
		// Clear the input file
		self.$('#file').val('');
		self.$('#input-file').val('');
	},

	renderMap : function() {
		var routePathCoordinates = [];
		var bounds = new google.maps.LatLngBounds();
		
		for (position in finalRuta.coordenadas) {
			routePathCoordinates.push({
				lat : parseFloat(finalRuta.coordenadas[position].latitud),
				lng : parseFloat(finalRuta.coordenadas[position].longitud)
			})
		}

		routePath = new google.maps.Polyline({
			path : routePathCoordinates,
			strokeColor : '#009688',
			strokeOpacity : 1.0,
			strokeWeight : 2.0
		});
		
		routePath.setMap(mapCreate);
		zoomToObject(routePath);
		
		function zoomToObject(obj){
		    var bounds = new google.maps.LatLngBounds();
		    var points = obj.getPath().getArray();
		    for (var n = 0; n < points.length ; n++){
		        bounds.extend(points[n]);
		    }
		    mapCreate.fitBounds(bounds);
		}
	},

	events : {
		'click #button-guardar' : 'createRuta',
		'change #file' : 'readGPXFile',
		'fileselect #file' : 'fileSelect'
	},

	createRuta : function() {
		this.$('#progress-create').removeClass('hide');
		this.model = new Ruta();
		
		//Get all the values just if the user has changed
		this.model.set('nombre', this.$('#name').val());
		this.model.set('descripcion', this.$('#ruta-description').val());
		this.model.set('dificultad',this.$('#ruta-dificult').val());
		this.model.set('distancia', finalRuta.distancia);
		this.model.set('esCircular', this.$('#ruta-circular').val());
		this.model.set('coordenadas', finalRuta.coordenadas);

		console.log(this.model);
		// save information into the collection if the phone is valid
		this.collection.add(this.model);
		// clear the model
		this.model = null;
		// render to reset all
		this.render();
		// Reset the file input
		var file_input = this.$('#input-file');
		file_input.replaceWith(file_input = file_input.clone(true));
		//Remove the polyline with the route
		routePath.setMap(null);
	},

	fileSelect : function(event, label) {
		this.$('#input-file').val(label);
	},

	readGPXFile : function(e) {
		var self = this;
		if (e) {
			if (window.File && window.FileReader && window.FileList
					&& window.Blob) {
				// Great success! All the File APIs are supported.
				// get the files selected
				var files = e.target.files;
				// get the label of the file
				var label = files.item(0).name;
				$(this).trigger('fileselect', [ label ]);
				// File Reader to read the file
				var reader = new FileReader();
				// On load method. read finished
				reader.onload = function(event) {
					var textXML = event.target.result;
					// Parse the txtXML into xml and parso to Json with the
					// function created
					var json_ruta = xmlToJson($.parseXML(textXML));

					// Into the final ruta we have the ruta data
					finalRuta = objectJSONparser(json_ruta);

					// Save the name of the ruta for the user to see it
					self.$('#name').val(finalRuta.nombre);
					Materialize.updateTextFields();

					// Save the dificulty of the ruta for the user to see it
					self.$('#ruta-dificult').val(finalRuta.dificultad);
					// Call the select item to initialize
					$(document).ready(function() {
						$('select').material_select();
					});

					// Enable button
					self.$('#button-guardar').addClass(
							"waves-effect waves-light submit").removeClass(
							'disabled');
					self.renderMap();
				}
				// Read the file
				reader.readAsText(files[0]);
			} else {
				Materialize.toast(BROWSER_NOT_UPDATED, 2000);
			}
		}
	}

});
