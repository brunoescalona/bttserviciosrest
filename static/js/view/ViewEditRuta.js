/**
 * View Edit Ruta
 */

var ViewEditRuta = Backbone.View.extend({
	
	initialize : function() {
		var self = this;
		this.collection.on('remove', function() {
			self.renderSelectItem();
		});
		this.collection.on('sync', function() {
			self.renderSelectItem();
		});
		self.renderSelectItem();
	},
	
	//Render the selected item from the select box
	renderSelectItem : function() {
		var self = this;
		// delete the items contained
		this.$('#select-ruta-edit').children().remove();
		// render all the info in the data base into the selector
		for (var i = 0; i < this.collection.size(); i++) {
			var mRuta = this.collection.at(i);
			var str = '<option value=' + mRuta.id + '>' + mRuta.get('nombre')
					+ '</option>'
			self.$('#select-ruta-edit').append(str);
		}
		this.$('#select-ruta-edit').material_select();
		//Call the select item to initialize
		$(document).ready(function() {
			$('select').material_select();
		});
		this.render();
	},
	
	//Render the view with all the data from the model item
	render : function() {
		var self = this;
		//  get the value of the item selected
		var idRuta = this.$('#select-ruta-edit').val();
		//If there is no elements clean all the data
		if(!idRuta){
			self.$('#ruta-name').val('');
			self.$('#ruta-dificult-edit').val('Facil');
			self.$('#textarea-description').val('');
		}else{
			// Set all the files with the value designed
			self.$('#ruta-name')
					.val(this.collection.get(idRuta).get('nombre'));
			self.$('#ruta-dificult-edit').val(this.collection.get(idRuta).get('dificultad'));
			self.$('#textarea-description').val(this.collection.get(idRuta).get('descripcion'));
			// re-initialize material-select
			self.$('#select-ruta-edit').material_select();
		}
		//re-initialize material text fields
		Materialize.updateTextFields();
		this.renderMap(idRuta);
	},
	
	renderMap : function(idRuta) {
		if(routePath){
			//Remove the polyline with the route
			routePath.setMap(null);
		}

		var routePathCoordinates = [];
		var bounds = new google.maps.LatLngBounds();
		
		for (position in this.collection.get(idRuta).get('coordenadas')) {
			routePathCoordinates.push({
				lat : parseFloat(this.collection.get(idRuta).get('coordenadas')[position].latitud),
				lng : parseFloat(this.collection.get(idRuta).get('coordenadas')[position].longitud)
			})
		}
		
		routePath = new google.maps.Polyline({
			path : routePathCoordinates,
			strokeColor : '#009688',
			strokeOpacity : 1.0,
			strokeWeight : 2.0
		});
		
		routePath.setMap(mapEdit);
		zoomToObject(routePath);
		
		function zoomToObject(obj){
		    var bounds = new google.maps.LatLngBounds();
		    var points = obj.getPath().getArray();
		    for (var n = 0; n < points.length ; n++){
		        bounds.extend(points[n]);
		    }
		    mapEdit.fitBounds(bounds);
		}
	},
	
	events : {
		'change #select-ruta-edit' : 'getRutaSelected',
		'click #button-eliminar': 'deleteRuta',
		'click #button-editar': 'modifyRuta'
	},

	// Get the info from the element selected
	getRutaSelected : function() {
		this.render();
	},
	
	// Delete the ruta selected
	deleteRuta : function() {
		//  get the value of the item selected
		var idRuta = this.$('#select-ruta-edit').val();
		this.collection.remove(idRuta);
		this.render();
	},
	
	// Modify the ruta selected
	modifyRuta : function() {
		var self = this;
		//  get the value of the item selected
		var idRuta = this.$('#select-ruta-edit').val();
		var model = this.collection.get(idRuta);
		
		var nameRuta = this.$('#ruta-name').val();
		var dificultRuta = this.$('#ruta-dificult-edit').val();
		var descriptionRuta = this.$('#textarea-description').val();
		
		var data = {nombre:nameRuta,
				dificultad:dificultRuta,
				descripcion:descriptionRuta,};
		
		//Save the model after take all the changes from the user
		model.save(data,{
			success: function(model, response) {
	        	Materialize.toast(RUTA_MODIFIED_CORRECT, 2000);
	        },
	        error: function(model, response) {
	        	Materialize.toast(RUTA_MODIFIED_INCORRECT, 2000);
	        },
	        wait: true
		});
		
		this.render();
	}
});
