/**
 * View Create Info
 */
var ViewCreateInfo = Backbone.View.extend({
	initialize : function() {
		var self = this;
		this.collection.on('remove', function() {
			self.render();
		});
		this.collection.on('sync', function() {
			self.render();
		});
		self.render();
	},
	
	//Render all in blank after submit
	render : function() {
		this.$('#name-info-create').val('');
		this.$('#create-info-type').val('1');
		//re-initialize material input
		this.$('#input-info-type').material_select();
		this.$('#telf-info-create').val('');
		this.$('#dir-info-create').val('');
		//re-initialize material text fields
		Materialize.updateTextFields();
		//Call the select item to initialize
		$(document).ready(function() {
			$('select').material_select();
		});
	},
	
	events : {
		'click #button-guardar-info': 'createInfo'
	},
	
	createInfo: function (){
		//Create a new Info 
		this.model = new Info();
		//Check if the user has introduced some info
		var nameInfo = this.$('#name-info-create').val();
		var tipeInfo = getTypeFromNumber(this.$('#create-info-type').val());
		var iconInfo = getIconFromNumber(this.$('#create-info-type').val());
		var dirInfo = this.$('#dir-info-create').val();
		var phoneInfo = this.$('#telf-info-create').val();
		//If true all the form is filled.
		if (validateCreateInfo(nameInfo, dirInfo, phoneInfo)){
			//Check if the phone number is valid
			var phone = validatePhoneNumber(phoneInfo);
			if(phone){
				//Set nombre
				this.model.set('nombre', nameInfo);
				//Set tipo
				this.model.set('tipo', tipeInfo);
				//Set telf
				this.model.set('telf', phone);
				//Set direccion
				this.model.set('direccion', dirInfo);
				//Set the icon
				this.model.set('icono', iconInfo);
				// save information into the collection if the phone is valid
				this.collection.add(this.model);
				//clear the model
				this.model = null;
				//render to reset all
				this.render();
			}else{
				//Set the phone to empty
				this.$('#telf-info-create').val('');
				//re-initialize material text fields
				Materialize.updateTextFields();
				Materialize.toast(INFO_PHONE_NOT_VALID, 2000);
			}
		}else{
			Materialize.toast(INFO_EMPTY, 2000);
		}
	}
});
