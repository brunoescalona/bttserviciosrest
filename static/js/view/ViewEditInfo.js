/**
 * View Edit Info
 */

var ViewEditInfo = Backbone.View.extend({

	initialize : function() {
		var self = this;
		this.collection.on('remove', function() {
			self.renderSelectItem();
		});
		this.collection.on('sync', function() {
			self.renderSelectItem();
		});
		self.renderSelectItem();
	},
	
	//Render the selected item from the select box
	renderSelectItem : function() {
		var self = this;
		// delete the items contained
		this.$('#select-info-edit').children().remove();
		// render all the info in the data base into the selector
		for (var i = 0; i < this.collection.size(); i++) {
			var mInfo = this.collection.at(i);
			var str = '<option value=' + mInfo.id + '>' + mInfo.get('nombre')
					+ '</option>'
			self.$('#select-info-edit').append(str);
		}
		this.$('#select-info-edit').material_select();
		//Call the select item to initialize
		$(document).ready(function() {
			$('select').material_select();
		});
		this.render();
	},
	
	//Render the view with all the data from the model item
	render : function() {
		var self = this;
		//  get the value of the item selected
		var idInfo = this.$('#select-info-edit').val();
		//If there is no elements clean all the data
		if(!idInfo){
			self.$('#name-info-edit').val('');
			self.$('#telf-info-edit').val('');
			self.$('#dir-info-edit').val('');
			self.$('#input-info-type').val("1");
		}else{
			// Set all the files with the value designed
			self.$('#name-info-edit')
					.val(this.collection.get(idInfo).get('nombre'));
			self.$('#telf-info-edit').val(this.collection.get(idInfo).get('telf'));
			self.$('#dir-info-edit').val(this.collection.get(idInfo).get('direccion'));
			// Get the type of info
			self.$('#input-info-type').val(getNumberFromType(this.collection.get(idInfo).get('tipo')));
			// re-initialize material-select
			self.$('#input-info-type').material_select();
		}
		//re-initialize material text fields
		Materialize.updateTextFields();
	},

	events : {
		'change #select-info-edit' : 'getInfoSelected',
		'click #button-eliminar-info': 'deleteInfo',
		'click #button-editar-info': 'modifyInfo'
	},

	// Get the info from the element selected
	getInfoSelected : function() {
		this.render();
	},
	
	// Delete the info selected
	deleteInfo : function() {
		//  get the value of the item selected
		var idInfo = this.$('#select-info-edit').val();
		this.collection.remove(idInfo);
		this.render();
	},
	
	// Modify the info selected
	modifyInfo : function() {
		var self = this;
		//  get the value of the item selected
		var idInfo = this.$('#select-info-edit').val();
		var model = this.collection.get(idInfo);
		
		var nameInfo = this.$('#name-info-edit').val();
		var tipeInfo = getTypeFromNumber(this.$('#input-info-type').val());
		var dirInfo = this.$('#dir-info-edit').val();
		var phoneInfo = this.$('#telf-info-edit').val();
		var iconInfo = getIconFromNumber(this.$('#input-info-type').val());
		//If true all the form is filled.
		if (validateCreateInfo(nameInfo, dirInfo, phoneInfo)){
			//Check if the phone number is valid
			var phone = validatePhoneNumber(phoneInfo);
			if(phone){
				//Save all the data introduced by the user
				//Set the new Telf info
				//Set the new adress
				//Set the type of info
				var data = {nombre:nameInfo,
							telf:phoneInfo,
							direccion:dirInfo,
							tipo:tipeInfo,
							icono:iconInfo};
				
				//Save the model after take all the changes from the user
				model.save(data,{
					success: function(model, response) {
			        	Materialize.toast(INFO_MODIFIED_CORRECT, 2000);
			        },
			        error: function(model, response) {
			        	Materialize.toast(INFO_MODIFIED_INCORRECT, 2000);
			        },
			        wait: true
				});
				
				this.render();
				
			}else{
				//Set the phone to empty
				this.$('#telf-info-edit').val('');
				//re-initialize material text fields
				Materialize.updateTextFields();
				Materialize.toast(INFO_PHONE_NOT_VALID, 2000);
			}
		}else{
			Materialize.toast(INFO_EMPTY, 2000);
		}
	}
});