/**
 * Functions to validate the input forms
 */
//Validate the phone number
// Return the phone validated or false if not
function validatePhoneNumber(phone)  
{  
  var phoneNum = phone.replace(/[^\d]/g, '');
  if(phoneNum.length > 2 && phoneNum.length < 10) {
      return phoneNum;  
  } else  {   
      return false;  
  }  
}  

//Validate that the fiels are not empty
function validateCreateInfo(name, dir, phone){
	if((name==null || name=="") || (dir==null || dir=="") || (phone==null || phone=="")){
		return false;
	}else{
		return true;
	}
}