/**
 * Alert Messages show in the page
 * Put here to have a faster modifications
 */
//INFO ALERT MESSAGES
var INFO_SAVED_CORRECT = 'Información guardada correctamente';
var INFO_SAVED_INCORRECT = 'Error al crear la información';
var INFO_DELETED_CORRECT = 'Información eliminada correctamente';
var INFO_DELETED_INCORRECT = 'La Información no ha sido eliminada, vuelva a intentarlo';
var INFO_PHONE_NOT_VALID = 'Introduce un teléfono válido';
var INFO_EMPTY = 'Rellena la información';
var INFO_MODIFIED_CORRECT = 'Información modificada correctamente';
var INFO_MODIFIED_INCORRECT = 'Error al modificar la información';

//BROWSER NOT UPDATED
var BROWSER_NOT_UPDATED = 'Actualiza el navegador o usa Explorer 10 o superior para poder subir el archivo';
	
//INFO ALERT MESSAGES
var RUTA_SAVED_CORRECT = 'Ruta guardada correctamente';
var RUTA_SAVED_INCORRECT = 'Error al crear la ruta';
var RUTA_DELETED_CORRECT = 'Ruta eliminada correctamente';
var RUTA_DELETED_INCORRECT = 'La Ruta no ha sido eliminada, vuelva a intentarlo';
var RUTA_MODIFIED_CORRECT = 'Ruta modificada correctamente';
var RUTA_MODIFIED_INCORRECT = 'Error al modificar la Ruta';