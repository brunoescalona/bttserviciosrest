/**
 * Info Type constants
 */
var TELEFONOS_INTERES = "telefonos_interes"; // value 1
var ALQUILER_BICICLETAS = "alquiler_bicicletas"; // value 2 
var TIENDAS_BICICLETAS = "tiendas_bicicletas"; // value 3 
var ALOJAMIENTO = "alojamiento"; // value 4 
var RESTAURANTE = "restaurante"; // value 5 
/**
 * Icons type
 */
var BASE_URL_ICONS = "http://rutas-bttalcoi.rhcloud.com/public/icons/"
var TELEFONOS_INTERES_ICON = BASE_URL_ICONS+"ic_phone_black_36dp.png"; // value 1
var ALQUILER_BICICLETAS_ICON = BASE_URL_ICONS+"ic_directions_bike_black_36dp.png"; // value 2 
var TIENDAS_BICICLETAS_ICON = BASE_URL_ICONS+"ic_build_black_36dp.png"; // value 3 
var ALOJAMIENTO_ICON = BASE_URL_ICONS+"ic_hotel_black_36dp.png"; // value 4 
var RESTAURANTE_ICON = BASE_URL_ICONS+"ic_phone_black_36dp.png"; // value 5 

/**
 * Method to obtain the type from the number
 */
var getTypeFromNumber = function(val){
	switch (val) {
	case "1":
		return TELEFONOS_INTERES;
		break;
	case "2":
		return ALQUILER_BICICLETAS;
		break;
	case "3":
		return TIENDAS_BICICLETAS;
		break;
	case "4":
		return ALOJAMIENTO;
		break;
	case "5":
		return RESTAURANTE;
		break;
	}
}
/**
 * Method to obtain the icons from the number
 */
var getIconFromNumber = function(val){
	switch (val) {
	case "1":
		return TELEFONOS_INTERES_ICON;
		break;
	case "2":
		return ALQUILER_BICICLETAS_ICON;
		break;
	case "3":
		return TIENDAS_BICICLETAS_ICON;
		break;
	case "4":
		return ALOJAMIENTO_ICON;
		break;
	case "5":
		return RESTAURANTE_ICON;
		break;
	}
}
/**
 * Method to obtain the number from the type
 */
var getNumberFromType = function(val){
	switch (val) {
	case TELEFONOS_INTERES:
		return "1";
		break;
	case ALQUILER_BICICLETAS:
		return "2";
		break;
	case TIENDAS_BICICLETAS:
		return "3";
		break;
	case ALOJAMIENTO:
		return "4";
		break;
	case RESTAURANTE:
		return "5";
		break;
	}
}