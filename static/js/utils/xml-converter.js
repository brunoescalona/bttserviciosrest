/**
 * XML to JavaScript Object converter
 */
function xmlToJson(xml) {

	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
			obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for (var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof (obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof (obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}

	return obj;
}

/**
 * Take the values of the JavaScript Object nedeed to save in the data base
 */
function objectJSONparser(obj) {
	// Latitude and logitude
	var latitude;
	var longitude;
	// Elevation
	var elevation

	var data = {};
	
	var finalRoute = {};
	// Set the initial values
	finalRoute.nombre = obj.gpx.trk.name["#text"];
	finalRoute.descripcion = '';
	finalRoute.dificultad = 'Facil';
	finalRoute.distancia = 0.0;
	finalRoute.esCircular = false;
	finalRoute.coordenadas = [];

	
	var distanceTotal = 0.0;

	for (position in obj.gpx.trk.trkseg.trkpt) {
		// Auxiliar to calculate the distance with the coordinates
		if (position != 0) {
			var dist = getDistanceFromLatLonInKm(
					parseFloat(latitude),
					parseFloat(obj.gpx.trk.trkseg.trkpt[position]["@attributes"].lat),
					parseFloat(longitude),
					parseFloat(obj.gpx.trk.trkseg.trkpt[position]["@attributes"].lon));
			distanceTotal = distanceTotal + dist;
		}
		// Get the values from the gpx file
		latitude = obj.gpx.trk.trkseg.trkpt[position]["@attributes"].lat;
		longitude = obj.gpx.trk.trkseg.trkpt[position]["@attributes"].lon;
		elevation = obj.gpx.trk.trkseg.trkpt[position].ele["#text"];
		
		// save the data object into the final route object
		finalRoute.coordenadas.push({latitud:latitude, longitud:longitude, altitud: elevation});
		
	}	

	// Set the total distance
	finalRoute.distancia = distanceTotal.toFixed(2);
	finalRoute.dificultad = getDificultFromDistance(distanceTotal);

	return finalRoute;
}