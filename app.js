var express = require('express');
//Require bodyparser to parse the body request
var bodyParser = require('body-parser');
//Require de database constants
var data_base = require('./database/database_utils');
//Require our model file ./models/rutas.js
var Ruta = require('./models/rutas');
//Require our model file ./models/info.js
var Info = require('./models/info');
//Mongoose para realizar la conexión y gestión de la base de datos
var mongoose = require('mongoose');
//To convert the JSON to XML
var json2xml = require('./utils/json2xml');

var app = express();


//Static page to load the rutas content
app.use(express.static('static'));
//Static page to load the public information
app.use('/public',express.static('_public'));

//Body parser to parse the body into a json
app.use(bodyParser.json({limit: '50mb'}));


var URL_ENDPOINT = data_base.url_endpoint.toString();
var URL_DOCUMENT = data_base.url_document.toString();

mongoose.connect(URL_ENDPOINT + URL_DOCUMENT);

//CORS
app.use(function(req, res, next) {
	 res.header('Access-Control-Allow-Origin', "*");
	 res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
	 res.header('Access-Control-Allow-Headers', 'Content-Type');
	 if (req.method == 'OPTIONS') {
		 res.status(200).send();
	 } else {
	 next();
	 }
});

/**
 * ENDPOINT RELATED WITH THE RUTAS
 */
/**
 * POST /rutas
 * post a ruta
 * Format:{
 * 		nombre: String,
 * 		descripcion: String,
 * 		dificultad: String,
 * 		distancia: Number,
 * 		esCircular: Boolean,
 * 		coordenadas:[{
 * 			latitud: String,
 * 			longitud: String,
 * 			altitud: String,
 * 			precision: String,
 * 		}]
 * }
 */
app.post('/rutas', function(req, res) {
	//console.log('POST /rutas');
	var rutaModel = new Ruta(req.body);
	// Guardamos la ruta que está en el body del mensaje en la base de datos
	rutaModel.save(function (err, rutas) {
	  if (err) {
	    console.log(err);
	  } else {
	    res.send(rutas);
	  }
	});
});

/**
 * GET /rutas
 * Get all the rutas from the database
 */
app.get('/rutas', function(req, res) {
	// Get all the rutas from the data base
	Ruta.find(function (err, rutas) {
		  if (err) {
			    console.log(err);
		  } else {
			  //Format the ouput to an JSON Object
			  var rutas_ouput = {};
			  rutas_ouput.rutas = rutas;
			  res.send(rutas_ouput);
		  }
	});
});

/**
 * GET /rutas/:id
 * Get ruta by id
 */
app.get('/rutas/:id', function(req, res) {
	// find the ruta by id in the body
	Ruta.findOne({ '_id': req.params.id }, function (err, rutas){
		if (err) {
		      console.log(err);
		  } else {
			  //Format the ouput to an JSON Object
			  var rutas_ouput = {};
			  rutas_ouput.rutas = rutas;
			  res.send(rutas_ouput);
		  }
	});
});

/**
 * GET /rutas/:id/download
 * Donwload the file of the ruta by id
 */
app.get('/rutas/:id/download', function(req, res) {
	// find the ruta by id in the body
	Ruta.findOne({ '_id': req.params.id }, function (err, rutas){
		if (err) {
		      console.log(err);
		  } else {
			  var aux = json2xml(rutas);
			  //Format the ouput to an JSON Object
			  var filename = 'track<'+req.params.id+'>.gpx';
			  var mimetype = 'application/octet-stream';
			  res.setHeader('Content-Type', mimetype);
			  res.setHeader('Content-disposition','attachment; filename='+filename);
			  res.send(aux);
		  }
	});
});

/**
 * PUT /rutas/:id
 * Put ruta by id
 */
app.put('/rutas/:id', function(req, res) {
	console.log("put");
	// deleted the _id because if not the data cannot be put
	delete req.body._id;
	Ruta.findByIdAndUpdate({ '_id': req.params.id }, req.body, {new: true}, function (err, ruta) {
		  if (err) return handleError(err);
		  res.send(ruta);
	});
});

/**
 * DELETE /info/:id
 * delete ruta by id
 */
app.delete('/rutas/:id', function(req, res) {
	console.log("delete");
	// find each person with a last name matching 'Ghost'
	var query = Ruta.findOne({ '_id': req.params.id });
	query.remove().exec(function (err, ruta) {
		  if (err) return handleError(err);
		  var success = {response:"success"};
		  res.send(JSON.stringify(success));
	})
});

/**
 * ENDPOINT RELATED WITH THE GENERAL INFORMATION
 */
/**
 * POST /info
 * post a general info
 * 
 * Format:{
 * 		nombre: String,
 *      tipo: String,
 * 		telf: Number,
 * 		icono: String,
 * 		direccion:String
 * }
 */
app.post('/info', function(req, res) {
	console.log("post");
	var infoModel = new Info(req.body);
	// Guardamos la ruta que está en el body del mensaje en la base de datos
	infoModel.save(function (err, info) {
	  if (err) {
	    console.log(err);
	  } else {
	    res.send(info);
	  }
	});
});

/**
 * GET /info
 * Get all the info from the database
 */
app.get('/info', function(req, res) {
	console.log("get");
	// Get all the rutas from the data base
	Info.find(function (err, infos) {
		  if (err) {
			    console.log(err);
		  } else {
			  //Format the ouput to an JSON Object
			  var infos_ouput = {};
			  infos_ouput.info = infos;
			  res.send(infos_ouput);
		  }
	});
});

/**
 * GET /info/:id
 * Get info by id
 */
app.get('/info/:id', function(req, res) {
	console.log("get by id");
	// find the ruta by id in the body
	Info.findOne({ '_id': req.params.id }, function (err, infos){
		if (err) {
		      console.log(err);
		  } else {
			  //Format the ouput to an JSON Object
			  var infos_ouput = {};
			  infos_ouput.info = infos;
			  res.send(infos_ouput);
		  }
	});
});

/**
 * PUT /info/:id
 * Put info by id
 */
app.put('/info/:id', function(req, res) {
	console.log("put");
	// deleted the _id because if not the data cannot be put
	delete req.body._id;
	Info.findByIdAndUpdate({ '_id': req.params.id }, req.body, {new: true}, function (err, info) {
		  if (err) return handleError(err);
		  res.send(info);
	});
});

/**
 * DELETE /info/:id
 * delete info by id
 */
app.delete('/info/:id', function(req, res) {
	console.log("delete");
	// find each person with a last name matching 'Ghost'
	var query = Info.findOne({ '_id': req.params.id });
	query.remove().exec(function (err, ruta) {
		  if (err) return handleError(err);
		  var success = {response:"success"};
		  res.send(JSON.stringify(success));
	})
});

//IP adress and Port for openshift
var ipaddress =process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

//Start app
//app.listen(port, ipaddress);
app.listen(port);
